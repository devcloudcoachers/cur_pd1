/**
 *
 * @author Oriol
 * @description Pruebas clase
 * @since
 */
public class AccountTriggerHandler {
	/**
	 * @author Oriol
	 * @description Pruebas metodo
	 * @param lstNew
	 */
	public static void insertOppWhenInsertAccount(List<Account> lstNew) {
		//TriggerAccountSetting__c config = TriggerAccountSetting__c.getInstance();
		//if(config.IsActiveCreatedefaultOpp__c == true ){
		List<Opportunity> lstOpps = new List<Opportunity>();
		for (Account a : lstNew) {
			Opportunity opp = new Opportunity();
			opp.Name = 'Opp -  ' + a.Name;
			opp.StageName = 'Prospecting'; //config.DefaultStageName__c;
			opp.CloseDate = Date.today().addDays(30);
			opp.AccountId = a.Id;
			// insert opp; //ESTO ESTA MAL, MUY MAL. Error a la 151 Limit DML.
			lstOpps.add(opp);
		}
		insert lstOpps;
		//}
	}

	public static void insertTaskWhenInsertAccount(List<Account> lstNew) {
		List<Task> lstToInsert = new List<Task>();
		for (Account a : lstNew) {
			Task t = new Task();
			t.Subject = 'Review Account';
			t.ActivityDate = Date.Today().addDays(7);
			t.OwnerId = a.OwnerId;
			t.WhatId = a.Id;
			lstToInsert.add(t);
		}
		insert lstToInsert;
	}

	public static void notChangeAccountTypeWithOpenOpportunities(
		List<Account> lstOld,
		List<Account> lstNew,
		Map<Id, Account> mapNew
	) {
		// Preparo consulta
		Set<Id> setIDs = new Set<Id>();
		for (Integer i = 0; i < lstNew.size(); i++) {
			if (lstNew[i].Type != lstOld[i].Type) {
				setIDs.add(lstNew[i].Id);
			}
		}

		if (setIDs.size() > 0) {
			List<Account> lstAccounts = [
				SELECT Id
				FROM Account
				WHERE
					Id IN (
						SELECT AccountId
						FROM Opportunity
						WHERE IsClosed = false AND AccountId IN :setIDs
					)
			];
			if (lstAccounts.size() > 0) {
				for (Account acc : lstAccounts) {
					mapNew.get(acc.Id)
						.addError('No puedes cambiar el Type en Opp abiertas.');
				}
			}
		}
	}

	public static void closeAllOpportunitiesWhenInactiveAccount(
		List<Account> lstOld,
		List<Account> lstNew
	) {
		// Preparo consulta
		Set<Id> setIDs = new Set<Id>();
		for (Integer i = 0; i < lstNew.size(); i++) {
			if (lstNew[i].Active__c == 'No' && lstOld[i].Active__c != 'No') {
				setIDs.add(lstNew[i].Id);
			}
		}

		if (setIDs.size() > 0) {
			List<Opportunity> lstOpps = [
				SELECT Id
				FROM Opportunity
				WHERE AccountId IN :setIDs AND IsClosed = false
			];
			for (Opportunity opp : lstOpps) {
				opp.Stagename = 'Closed Lost';
			}
			update lstOpps;
		}
	}
}