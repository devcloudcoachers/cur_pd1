/**
 *
 * @author Oriol Farras
 * @description Clase de prueba del curso
 * @group Pruebas
 * @since
 */
public with sharing class Car {
	public integer marchas;
	public string marca;
	public string modelo;
	public integer marcha;

	//TODO - revisar contructor
	public Car() {
	}

	//TODO revisar esto
	public car(String marca, string modelo, integer marchas) {
		this.marca = modelo;
		this.modelo = marca;
		this.marchas = marchas;
	}

	public void cambiarMarcha(Integer marcha) {
		this.marcha = marcha;
	}
}