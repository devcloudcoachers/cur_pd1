public with sharing class ContactTriggerHandler {
	public void execute() {
		if (Trigger.isExecuting) {
			if (Trigger.isBefore) {
				// before
				if (Trigger.isInsert) {
					//before insert
					System.debug('before insert');
				} else if (Trigger.isUpdate) {
					//before update
					System.debug('before update');
				} else if (Trigger.isDelete) {
					//before delete
                    System.debug('before delete');
				}
			} else {
				// after
				if (Trigger.isInsert) {
					//after insert
					System.debug('after insert');
				} else if (Trigger.isUpdate) {
					//after update
					System.debug('after update');
				} else if (Trigger.isDelete) {
					//after delete
					System.debug('after delete');
				} else if (Trigger.isUndelete) {
					//after undelete
					System.debug('after undelete');
				}
			}

			// Only available in insert and update triggers,
			// and the records can only be modified in before triggers.
			List<Contact> listNewRecords = Trigger.new;
			System.debug('listNewRecords-->' + listNewRecords);

			// Only available in update and delete triggers.
			List<Contact> listoldRecords = Trigger.old;
			System.debug('listoldRecords-->' + listoldRecords);

			// Only available in before update, after insert,
			// and after update triggers.
			//System.debug('mapNewRecords-->' + mapNewRecords);

			// Only available in update and delete triggers.
			//Map<ID, Contact> mapOldRecords = Trigger.oldMap;
			//System.debug('mapOldRecords-->' + mapOldRecords);

			Integer recordsSize = Trigger.size;
			System.debug('recordsSize-->' + recordsSize);
		}
	}
}