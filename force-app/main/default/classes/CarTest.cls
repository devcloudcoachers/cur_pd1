@istest
public with sharing class CarTest {
	@istest
	static void CarTest() {
		Car Volvo = new CAr();
		System.assertEquals(Volvo.marca, null);
		Car alfaromeo = new Car('Alfa', 'Giuleta', 7);
		alfaromeo.cambiarMarcha(3);
		System.assertEquals(alfaromeo.marcha, 3, 'No te la marxa correcta');
		System.assertEquals(alfaromeo.modelo, 'Giuleta');
	}
}