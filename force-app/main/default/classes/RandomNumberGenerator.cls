/**
 *
 * @author O.F.R
 * @since 2020
 */
public with sharing class RandomNumberGenerator {
	@invocablemethod
	public static List<Long> getRandomNumber(List<RandomNumberRange> ranges) {
		List<Long> randomNumberList = new List<Long>();

		for (RandomNumberRange range : ranges) {
			Long randomNumber = (range.min +
			(Long) (Math.random() * ((range.max - range.min) + 1)));
			randomNumberList.add(randomNumber);
		}

		return randomNumberList;
	}

	public class RandomNumberRange {
		@invocablevariable(required=true)
		public integer min;

		@invocablevariable(required=true)
		public integer max;
	}
}