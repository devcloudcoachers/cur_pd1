@istest
private class Book_Test {
	@istest
	static void testBookClass() {
		Book myBook = new Book();
		myBook.title = 'The Lord of the Rings';
		myBook.author = 'J.R.R. Tolkien';
		myBook.stock = 100;
		System.assertEquals(
			'The Lord of the Rings',
			myBook.title,
			'No corresponde el titulo'
		);
		System.assertEquals(
			'J.R.R. Tolkien',
			myBook.author,
			'No corresponde el autor'
		);
		System.assertEquals(100, myBook.stock, 'No corresponde el stock');

		myBook.updateStock(20);
		System.assertEquals(80, myBook.stock, 'No corresponde el stock');

		Book myOtherBook = new Book('The Witcher', 'Andrzej Sapkowski', 50);
		System.assertEquals(
			'The Witcher',
			myOtherBook.title,
			'No corresponde el titulo'
		);
		System.assertEquals(
			'Andrzej Sapkowski',
			myOtherBook.author,
			'No corresponde el autor'
		);
		System.assertEquals(50, myOtherBook.stock, 'No corresponde el stock');
	}
}