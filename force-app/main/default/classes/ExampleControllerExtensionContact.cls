public with sharing class ExampleControllerExtensionContact {
	public Contact con { get; set; }
	public Account acc { get; private set; }

	public ExampleControllerExtensionContact(
		ApexPages.StandardController stdCtr
	) {
		if (!Test.isRunningTest()) {
			stdCtr.addFields(
				new List<String>{
					'Id',
					'Name',
					'LastName',
					'FirstName',
					'AccountId'
				}
			);
		}
		con = (Contact) stdCtr.getRecord();

		acc = [
			SELECT Id, Name, Industry
			FROM Account
			WHERE Id = :con.AccountId
		];
	}
}