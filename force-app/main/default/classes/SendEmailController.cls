public with sharing class SendEmailController {
	public string toMail { get; set; }
	public string ccMail { get; set; }
	public string repMail { get; set; }
	private final Invoice__c invoice;

	public SendEmailController() {
	}

	public void sendMail() {
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		string[] to = new List<string>{ toMail };
		string[] cc = new List<string>{ ccMail };

		email.setToAddresses(to);
		if (ccMail != null && ccMail != '')
			email.setCcAddresses(cc);
		if (repmail != null && repmail != '')
			email.setInReplyTo(repMail);

		email.setSubject('Invoice');

		email.setHtmlBody(
			'Hello, <br/><br/>This is your invoice.' +
			'<br/><br/>Regards<br/> Developer'
		);
		try {
			List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
			// Reference the attachment
			PageReference pdf = Page.InvoiceDocumentPage;
			String invoiceId = apexpages.currentpage()
				.getparameters()
				.get('id');

			pdf.getParameters().put('id', invoiceId);
			pdf.setRedirect(true);

			// Take the PDF content
			Blob b = pdf.getContent();
			System.debug('ID-->' + invoiceId);

			// Create the email attachment
			Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
			efa.setFileName('invoice.pdf');
			efa.setBody(b);
			fileAttachments.add(efa);
			email.setFileAttachments(fileAttachments);

			Messaging.sendEmail(
				new List<Messaging.SingleEmailMessage>{ email }
			);
		} catch (exception e) {
			apexpages.addmessage(
				new apexpages.message(apexpages.severity.error, e.getMessage())
			);
		}

		toMail = '';
		ccMail = '';
		repMail = '';
	}
}