public class InvoiceDocumentPage_EXT {
    public Invoice__c invoice{get;set;}
	public InvoiceDocumentPage_EXT(ApexPages.StandardController stdController) {
        this.invoice = (Invoice__c)stdController.getRecord();
        this.invoice = [SELECT Id, Name, Invoice_Due_Date__c,Total_Gross__c,CreatedDate,Account__r.Name,Contact__r.Name,Contact__r.Email,(SELECT Id,Quantity__c,Description__c,unit_price__c,IVA__c,Discount__c,Total__c FROM Invoice_Lines__r) FROM Invoice__c Where Id=:this.invoice.Id ];
    }
}