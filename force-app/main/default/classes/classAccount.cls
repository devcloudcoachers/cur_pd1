public with sharing class classAccount {
	private Account acc = null;

	public classAccount() {
		acc = new Account();
	}

	public classAccount(String NameAcc) {
		acc = new Account(Name = NameAcc);
	}

	public Account getAccount() {
		return acc;
	}

	public static Integer numberOfAccounts() {
		return [SELECT COUNT() FROM Account];
	}
}