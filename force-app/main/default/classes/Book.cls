public with sharing class Book {
	//Class variables
	public String title;
	public String author;
	public Integer stock;

	//This contstructor creates a book with predefined values
	public Book() {
		title = 'Unnamed book';
		author = 'Anonymous author';
		stock = 0;
	}

	//This constructor creates a book with values passed by parameters
	public Book(String title, String author, Integer stock) {
		this.title = title;
		this.author = author;
		this.stock = stock;
	}

	//This method subtract the value passed by parameter to the class variable
	public void updateStock(Integer bookSold) {
		this.stock = this.stock - bookSold;
	}
}