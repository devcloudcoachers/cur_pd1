@isTest
public with sharing class AccountTriggerHandlerTest {
	@IsTest
	static void testAccountTriggerAfterInsert() {
		Account acct = new Account(
			Name = 'Test triggers',
			AccountNumber = '1234567890'
		);
		insert acct;

		//Validar la creacion de la tarea
		List<Task> tasks = [SELECT id FROM task WHERE whatid = :acct.id];
		System.assertEquals(tasks.size(), 1, 'No se ha creado la tarea');
	}

	@IsTest
	static void testafterupdateValidation() {
		Account acct = new Account(
			Name = 'Test triggers 2',
			AccountNumber = '1234567891'
		);
		insert acct;

		//Validar que no se produce el error
		try {
			acct.Type = 'Other';
			update acct;
		} catch (Exception e) {
			System.debug('Entra en la exception');
			system.assert(
				e.getMessage()
					.contains('No puedes cambiar el Type en Opp abiertas')
			);
		}
	}

	@IsTest
	static void testafterupdateCloseOpps() {
		Account acct = new Account(
			Name = 'Test triggers 3',
			AccountNumber = '1234567892'
		);
		insert acct;

		acct.Active__c = 'No';
		update acct;

		List<Opportunity> opps = [
			SELECT ID
			FROM Opportunity
			WHERE AccountId = :acct.id AND Stagename != 'Closed Lost'
		];
		System.assertEquals(opps.size(), 0, 'Hay oportunidades abiertas');
	}
}