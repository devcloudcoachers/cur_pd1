public class ContactSearch {
	public static List<contact> searchForContacts(
		String firstName,
		String zipCode
	) {
		List<Contact> contacts = [
			SELECT ID, name
			FROM Contact
			WHERE firstname = :firstName AND MailingPostalCode = :zipCode
		];
		return contacts;
	}
}