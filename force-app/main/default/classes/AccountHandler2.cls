public with sharing class AccountHandler2 {
	public static List<Account> insertDatabaseAccounts(Integer numberAcct) {
		List<Account> acctList = new List<Account>();
		for (Integer i = 0; i <= numberAcct; i++) {
			if (i / 2 == 0) {
				acctList.add(new Account());
			} else {
				acctList.add(
					new Account(
						Name = 'Acme' + i,
						AccountNumber = '123456789' + i
					)
				);
			}
		}

		// DML statement
		// insert acctList;
		// Database Class Methods
		Database.SaveResult[] srList = Database.insert(acctList, false);
		for (Database.SaveResult sr : srList) {
			if (sr.isSuccess()) {
				System.debug('Successfully inserted. AccountId: ' + sr.getId());
			} else {
				for (Database.Error err : sr.getErrors()) {
					System.debug('The following error has occurred.');
					System.debug(err.getStatusCode() + ': ' + err.getMessage());
					System.debug('Account fields affected: ' + err.getFields());
				}
			}
		}

		return acctList;
	}
}