import { LightningElement, api, wire, track } from "lwc";
import {
	getRecord,
	getFieldValue,
	createRecord,
	deleteRecord
} from "lightning/uiRecordApi";
import TODO_OBJECT from "@salesforce/schema/Todo__c";
import DESCRIPTION_FIELD from "@salesforce/schema/Todo__c.Description__c";
import DUEDATE_FIELD from "@salesforce/schema/Todo__c.Due_Date__c";
import REMINDER_FIELD from "@salesforce/schema/Todo__c.Reminder_Date__c";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { updateRecord } from "lightning/uiRecordApi";

export default class ToDoItem extends LightningElement {
	@api todoId;
	@track edit;
	@track error;
	@track todo;
	description_field;
	reminder_field;
	duedate_field;

	@wire(getRecord, {
		recordId: "$todoId",
		fields: [DESCRIPTION_FIELD, DUEDATE_FIELD, REMINDER_FIELD]
	})
	wiredTodo({ error, data }) {
		if (data) {
			this.todo = data;
			this.edit = false;
			this.error = undefined;
		} else if (error) {
			this.error = error;
			this.todo = undefined;
			this.edit = true;
		} else {
			this.edit = true;
		}
	}

	get description() {
		return this.todo ? getFieldValue(this.todo, DESCRIPTION_FIELD) : "";
	}

	get due_date() {
		return this.todo ? getFieldValue(this.todo, DUEDATE_FIELD) : "";
	}

	get reminder_date() {
		return this.todo ? getFieldValue(this.todo, REMINDER_FIELD) : "";
	}

	handleChange(event) {
		switch (event.target.name) {
			case "description":
				this.description_field = event.target.value;
				break;
			case "due-date":
				this.duedate_field = event.target.value;
				break;
			case "reminder-due-date":
				this.reminder_field = event.target.value;
				break;
			default:
				break;
		}
	}

	handleEdit() {
		this.edit = true;
	}

	handleDelete() {
		deleteRecord(this.todoId)
			.then(() => {
				this.dispatchEvent(
					new ShowToastEvent({
						title: "Success",
						message: "Todo deleted",
						variant: "success"
					})
				);
				this.dispatchEvent(new CustomEvent("deleted"));
			})
			.catch(error => {
				this.dispatchEvent(
					new ShowToastEvent({
						title: "Error deleting record",
						message: error.body.message,
						variant: "error"
					})
				);
			});
	}

	handleSave() {
		if (this.todoId) {
			this.updateTodo();
		} else {
			this.createTodo();
		}
	}

	createTodo() {
		const fields = {};
		fields[DESCRIPTION_FIELD.fieldApiName] = this.description_field;
		fields[DUEDATE_FIELD.fieldApiName] = this.duedate_field;
		fields[REMINDER_FIELD.fieldApiName] = this.reminder_field;
		const recordInput = { apiName: TODO_OBJECT.objectApiName, fields };
		createRecord(recordInput)
			.then(todo => {
				this.todoId = todo.id;
				this.dispatchEvent(
					new ShowToastEvent({
						title: "Success",
						message: "Todo created",
						variant: "success"
					})
				);
				this.dispatchEvent(new CustomEvent("saved"));
			})
			.catch(error => {
				this.dispatchEvent(
					new ShowToastEvent({
						title: "Error creating record",
						message: error.body.message,
						variant: "error"
					})
				);
			});
	}

	updateTodo() {
		const allValid = [
			...this.template.querySelectorAll("lightning-input")
		].reduce((validSoFar, inputFields) => {
			inputFields.reportValidity();
			return validSoFar && inputFields.checkValidity();
		}, true);

		if (allValid) {
			// Create the recordInput object
			const fields = {};
			fields[ID_FIELD.fieldApiName] = this.contactId;
			fields[FIRSTNAME_FIELD.fieldApiName] = this.template.querySelector(
				"[data-field='FirstName']"
			).value;
			fields[LASTNAME_FIELD.fieldApiName] = this.template.querySelector(
				"[data-field='LastName']"
			).value;

			const recordInput = { fields };

			updateRecord(recordInput)
				.then(() => {
					this.dispatchEvent(
						new ShowToastEvent({
							title: "Success",
							message: "Contact updated",
							variant: "success"
						})
					);
				})
				.catch(error => {
					this.dispatchEvent(
						new ShowToastEvent({
							title: "Error creating record",
							message: error.body.message,
							variant: "error"
						})
					);
				});
		} else {
			// The form is not valid
			this.dispatchEvent(
				new ShowToastEvent({
					title: "Something is wrong",
					message: "Check your input and try again.",
					variant: "error"
				})
			);
		}
	}
}
