import { LightningElement, track, wire } from "lwc";
import { refreshApex } from "@salesforce/apex";
import loadData from "@salesforce/apex/ToDoController.loadData";

export default class ToDoList extends LightningElement {
	@track newTodo = false;

	handleClick() {
		this.newTodo = !this.newTodo;
	}

	handleSaved() {
		refreshApex(this.todos);
		this.newTodo = false;
	}

	handleDeleted() {
		refreshApex(this.todos);
	}

	@wire(loadData)
	todos;
}
