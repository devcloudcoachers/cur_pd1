trigger ContactTrigger on Contact(
	before insert,
	before update,
	before delete,
	after update,
	after insert,
	after delete,
	after undelete
) {
	ContactTriggerHandler triggerhandler = new ContactTriggerHandler();
	triggerhandler.execute();
}