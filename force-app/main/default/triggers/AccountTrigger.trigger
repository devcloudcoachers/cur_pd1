trigger AccountTrigger on Account(
	before insert,
	before update,
	before delete,
	after insert,
	after update,
	after delete,
	after undelete
) {
	if (Trigger.isBefore) {
		System.debug('Trigger Account Before');
		if (Trigger.isInsert) {
			System.debug('INSERT');
		} else if (Trigger.isUpdate) {
			System.debug('UPDATE');
			AccountTriggerHandler.notChangeAccountTypeWithOpenOpportunities(
				Trigger.old,
				Trigger.new,
				Trigger.newMap
			);
		} else if (Trigger.isDelete) {
			System.debug('DELETE');
		}
	} else {
		System.debug('Trigger Account After');
		if (Trigger.isInsert) {
			System.debug('INSERT');
			AccountTriggerHandler.insertTaskWhenInsertAccount(Trigger.new);
			AccountTriggerHandler.insertOppWhenInsertAccount(Trigger.new);
		} else if (Trigger.isUpdate) {
			System.debug('UPDATE');
			AccountTriggerHandler.closeAllOpportunitiesWhenInactiveAccount(
				Trigger.old,
				Trigger.new
			);
		} else if (Trigger.isDelete) {
			System.debug('DELETE');
		} else if (Trigger.isUnDelete) {
			System.debug('UNDELETE');
		}
	}
}